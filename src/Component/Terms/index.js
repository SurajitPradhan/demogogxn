import React, { Component } from 'react'
import './style.css';
class Terms extends Component {
    render() {
        return (
            <div className="terms ">
                <h1 className="term mt-5">TERMS & CONDITIONS</h1>

                <p className="termsheading">Terms and conditions</p>
                <p className="des">Kindly read the entire terms and conditions precisely. 
                These are the principal information that will help you to understand your legal rights on this website.</p>
                <p className="descpara">www.gogxn.com is an e-commerce website which is managed by Bukalo India Pvt Ltd. If you are using/accessing/
                    purchasing/reading on this website then you are considered concurred with the following terms and conditions 
                    listed below. If your any of the activity will violate the company’s terms than you are alone responsible for
                     your actions and their consequences.  We would be glad to resolve your query about our terms and conditions.
                      So, please contact us at email- support@gogxn.com or call on +91-8447587080. We are not obliged to inform you
                     about our terms and conditions amendments as you are free to visit our websites as often as possible.</p>

               <p className="copyright_heading">Copyright</p>      
               <p className="copyright_text">All content comprises this site, such as text, graphics, logos, button icons, images, digital downloads and 
                   data compilations is the property of gogxn.com and protected by copyright laws. The compilation of all 
                   content on this site is the exclusive property of gogxn.com and no user either national or international 
                   allowed to 
                   use our specific content/facts/images for their own or anyone else collective/individual uses.</p>

                <p className="eligibility_heading">Eligibility and Restrictions</p>  
                <p className="eligibility_text px-4">
                <ol className="eligibility_list">
                    <li className="eligibility">You are eligible to use this website for the legit purpose only.</li>
                    <li className="eligibility">The device you are using for the accessing of this website is must not be involved in any kind of unlawful act.</li>
                    <li className="eligibility">You are eligible to perform the activities on a website like Visit, chat, purchase, and review only and you are strictly prohibited to modify or edit the available content on the website.
                         If you found involved in any kind of restricted activity than it will lead you to the legal jurisdictions.</li>
                    <li className="eligibility">You shall not use this website to spread malicious, abusive, obscene, libelous, or any objectionable material.</li>
                    <li className="eligibility">None of the individuals are allowed to obstruct any other person’s use or enjoyment of the website.</li>
                    <li className="eligibility">You are strictly prohibited from framing, hotlinking and deep linking any content on the websites.</li>
                    <li className="eligibility">This is an offense if you violated or attempting to violate the integrity or security of the website or its content.</li>
                    <li className="eligibility">You are not allowed to deconstruct, decompile, decipher or reverse engineer any part of the website.</li>
                    <li className="eligibility">The website doesn’t allow transaction using cryptocurrency and any kind of digital or physical material which is not considered by reserve bank of India.</li>
                    <li className="eligibility">Each of the buyers is restricted to buy more/less than limited quantity and company reserve the right to cancel, 
                        modify and deny the order of disallowed quantity. *</li>

                </ol>  
                    </p> 

                    <p className="accuracy_heading">Accuracy and Authenticity of content</p>  
                <p className="accuracy_text px-4">
                <ol className="accuracy_list">
                    <li className="accuracy">We have taken all the required precaution to maintain the accuracy of the content.</li>
                    <li className="accuracy">Present content could be more relatable to published date and time and may not match the current timing.</li>
                    <li className="accuracy">The product listed on the website may or may be not available all the time and company not compelled to mention the same.</li>
                    <li className="accuracy">The weights, dimensions, and capacities of the listed product are approximate only. We have made our extreme effort to display the facts as accurate as possible.</li>
                    <li className="accuracy">The company can’t give you surety that colour of received product will match the colour you saw while purchasing as it majorly depends upon the monitor’s display.</li>
                    <li className="accuracy">Company content team welcomes your suggestion to improve the quality and accuracy of content. You are allowed to suggest text content only.*</li>
                </ol>
                    </p> 

                    <p className="payment_heading">Accuracy and Authenticity of content</p>  
                <p className="payment_text px-4">
                <ol className="payment_list">
                    <li className="payment">We accept payment through Net banking, credit card, Debit card, and COD*</li>
                    <li className="payment">The company can deny the delivery even after the order’s placement in the condition of product scarcity, quantity, the location of order or any other personal 
                        reason and we are not forced to give the clarification for that.</li>
                    <li className="payment">Delivery time could exceed the mentioned time limit and this reason would not be sufficient to deny the acceptance.</li>
                    <li className="payment">Price of the product is variable and it may vary for different buyers at the different</li>
                        </ol>
                    </p> 


                    <p className="career_contact">
                        
                    Contact Us </p>
                    <p className="ceo_details px-2">
                    <h6 className="ceo"><b>Mr. Sandeep Singh</b></h6>
                    <h6 className="ceo">Bukalo India Pvt. Ltd.</h6>
                    <h6 className="ceo">Phone: +91 8700351062</h6>
                    <h6 className="ceo">Email: support@gogxn.com</h6>
                    <h6 className="ceo">Time: Mon – Fri (9:30am – 7:30pm)</h6>
                    </p>

                
            </div>
        )
    }
}

export default Terms

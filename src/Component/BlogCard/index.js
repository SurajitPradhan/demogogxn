
import React, { Component } from 'react'
import { NavLink } from 'react-router-dom';
import './style.css';
const BlogCard =(props  )=> {
    
        return (


            <div class="card-group">
              
  <div class="blog_card card">
    <div className="inner">
    <img class="card-img-top" src={props.imgsrc}  alt="Responsive image"/></div>
    <div class="card-body">
      <h4 class="card-title " style={{color:"#333333"}}><b>{props.heading}</b></h4>
      <p className="text-muted">Leave a Comment</p>
      <p class="card-text">{props.text}</p>
      <a href="#" style={{color:"#333333"}}><b>{props.btn}</b> <i class="fa fa-caret-right" aria-hidden="true"></i>

</a>
    </div>
       
       
    
  </div>
</div>
           
               
               
          
        )
    
}

export default BlogCard

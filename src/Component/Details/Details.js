import React, { Component } from 'react'
import './style.css';

class Details extends Component {
    state={
        products:[
            {
                "_id": "1",
                "title": "GXN (GREENEX NUTRITION) N.O. PUMP – BEST PRE WORKOUT",
                "src": [
                    "https://gogxn.com/wp-content/uploads/2020/12/noPump30_Blueberry.png",
                    // "https://gogxn.com/wp-content/uploads/2020/12/noPump30dise02_final_4K-1.png",
                    "https://gogxn.com/wp-content/uploads/2020/12/noPump30_Blueberry.png",
                    "https://gogxn.com/wp-content/uploads/2020/12/noPump30_Blueberry.png",
                    "https://gogxn.com/wp-content/uploads/2020/12/noPump30_Blueberry.png",
                    
                    "https://gogxn.com/wp-content/uploads/2018/08/IMG-20200926-WA0008.jpg"
                
                  ],
                "description": "UI/UX designing, html css tutorials",
                "content": "Each serving of N.O.  Pump includes Citruline DL malate (1500 mg), Beta Alanine (3000mg), Creatine (1000 mg), and L Arginine (1500 mg) ",
                "price": 23,
                "colors":["red","black","crimson","teal"],
                "count": 1
                }
        
        ],
        index:0
    };

    myRef = React.createRef();

    handleTab= index=>{
        this.setState({index: index})
        const images = this.myRef.current.children;
        for(let i=0; i<images.length; i++){
            images[i].className= images[i].className.replace("active", "");
        }
        images[index].className="active";
        // console.log(this.myRef.current.children)
        // alert(index)
    }
    render() {
        const {products, index} = this.state;
        console.log(products)
        return (
            
                <div className="apps">
                    {
                        products.map(item =>(
                            <div className="details" key={item._id}>
                                <div className="big-img">
                                    <img src={item.src[index]} alt="/"/>    
                                    <div className="thumb" ref={this.myRef}>
                                          {
                                              item.src.map((img, index)=>(
                                                  <img src={img} alt="/" key={index}
                                                  onClick={() => this.handleTab(index)}/>
                                              ))
                                          }
                                          </div>                          
                                </div>
                                
                                <div className="box">
                                    <div className="row">

                                        <h2>{item.title}</h2>
                                        {/* <p>{item.content}</p> */}
                                     <p >  <span>${item.price}</span></p> 
                                        </div>
                                        
                                        <li>{item.content}</li>
                                        <li>{item.content}</li>
                                        <li>{item.content}</li>
                                        <li>{item.content}</li>
                                        <li>{item.content}</li>
                                        <li>{item.content}</li> <li>{item.content}</li> <li>{item.content}</li> <li>{item.content}</li>
                                     
                                      <div className="colors">
                                          {
                                              item.colors.map((color, index)=>(
                                                  <button style={{background:color}} key={index}></button>
                                              ))
                                          }
                                      </div>
                                      <p>{item.content}</p>
                                      <p>  {item.description}</p>
                                     

                                          <button className="cart">Add to Cart</button>
                                    </div>
                                    </div>
                        ))
                    }
                </div>
            
        )
    }
}

export default Details

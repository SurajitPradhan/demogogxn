import React, { Component } from 'react'
import './style.css'
class Footerstrip extends Component {
    render() {
        return (
            <div className="footerstrip">
<div class="row ">
  
  <div class="col-4 copyright">Copyright @2020 Greenex Nutrition by <br/>Bukalo India Private Limited</div>
  <div class="col-8 disclaimer">Disclaimer: All GXN products are manufactured at FSSAI approved manufacturing facilities and are not intended to diagnose, treat, cure, or prevent any disease. Please read product packaging carefully prior to purchase and use .</div>
</div>
            </div>
        )
    }
}

export default Footerstrip

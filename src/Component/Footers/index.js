import React from 'react';
// import './style.css';
const Footers=()=>{
    return(
        <div className="footers">
            <div className="footer-top">
                <div className="footer_container">
                    <div className="row">
                        <div className="col-md-3 col-sm-6 colxs-12  segment-one ">
                            <h4 className="about mt-4">ABOUT</h4>
                            <ul><li><a href="https://gogxn.com/blog/">Blog</a></li>
                            <li><a href="https://gogxn.com/faqs/">FAQs</a></li>
                            <li><a href="https://gogxn.com/my-account/">My Account</a></li>
                            <li><a href="https://gogxn.com/track-your-order/">Track Your Order</a></li>
                            <li><a href="https://gogxn.com/contact-us/">Customer Support</a></li></ul>
                        </div>

                        <div className="col-md-3 col-sm-6 colxs-12 segment-two">
                            <h4 className="about mt-4">KNOW MORE</h4>
                            <ul><li><a href="https://gogxn.com/become-a-dealer/">Become a Dealer</a></li>
                            <li><a href="https://gogxn.com/terms-conditions/">Terms & Conditions </a></li>
                            <li><a href="https://gogxn.com/privacy-policy/">Privacy & Policy</a></li>
                            <li><a href="https://gogxn.com/refunds-cancellations/">Refunds & Cancellations</a></li>
                            <li><a href="https://gogxn.com/careers/">Careers</a></li></ul>


                        </div>



                        <div className="col-md-3 col-sm-6 colxs-12 segment-three">
                            <h4 className="about mt-4">GET SOCIAL</h4>
                            <a href="https://www.facebook.com/gogxn/"><i className="fa fa-facebook"></i></a>
                            <a href="https://twitter.com/go_gxn"><i className="fa fa-twitter"></i></a>
                            <a href="https://www.flickr.com/people/158004145@N04/"><i className="fa fa-flickr"></i></a>
                            <a href="https://in.pinterest.com/go_gxn/"><i className="fa fa-pinterest"></i></a>
                            <a href="https://www.youtube.com/channel/UCa4cEu-G1AejDb27q66L5ag"><i className="fa fa-youtube"></i></a>

                        </div>


                        <div className="col-md-3 col-sm-6 colxs-12 segment-four">
                            <h4 className="about mt-4">SUBSCRIBE US</h4>
                            <form action="">
                            <input type="text" class="form-control " id="inputnamel4" placeholder="Name"/>
                            <input type="email" class="form-control mt-2" id="inputEmail4" placeholder="Email"/>
                            <button type="button" class="btn btn-primary mt-2">Subscribe</button>
                            </form> 

                        </div>
                    </div>
                </div>
            </div>
</div>    )
};
export default Footers;
import axios from 'axios'
import React, { Component } from 'react'

class Banner extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             name:[],
             image:[],
             activeset:''
        }
    }
    // http://127.0.0.1:8000/sliderimage/
    componentDidMount(){
        axios.get('http://127.0.0.1:8000/sliderimage/')
        .then(response=>{
            for (var i = 0; i < response.data.length; i++)
            this.setState({
                name:[...this.state.name, response.data[i].name],
                image: [...this.state.image, response.data[i].image],
                activeset:this.setState.activeset

            })
            console.log(response)
        })
    }
    render() {
        {
            this.state.image.map(function(image, index) {   
         console.log(image)      
        })
       }
        return (
            <div className="slider mt-5">
            <div id="slideshow" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner">
          { 
             this.state.image.map(function(image, index) {
                 console.log(index, image)
               if(index===1){
              return (        
                  <div class="carousel-item active" >
                    <img src={image} class="img-fluid d-block w-100" alt="Responsive image" width="1100" height="300"/>
              </div>
            );
              }
              else {
              return (        
               <div class="carousel-item" >
                   <img src={image} class="img-fluid d-block w-100" alt="Responsive image" width="1100" height="300"/>
             </div>
           ); 
              }
         }
         )
        }    
     </div>

     <a href="#slideshow" class="carousel-control-prev"
     role="button" data-slide="prev">
  <span class="carousel-control-prev-icon">
  </span>
<span class="sr-only">Previous</span>
</a>

<a href="#slideshow" class="carousel-control-next"
role="button" data-slide="next">
<span class="carousel-control-next-icon">
</span>
<span class="sr-only">Next</span></a>

  </div>
      </div>
        )
    }
}

export default Banner;


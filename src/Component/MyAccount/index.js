import React, { Component } from 'react'
import './style.css';
class MyAccount extends Component {
    render() {
        return (
            <div className="myaccount">
                <div className="row">
                    <div className="col-md-6">
                        <div className="login mt-5 ">
                        <form>
                          <div class="form-group">
                         <label for="exampleInputEmail1">Email address</label>
                         <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email"/ >
                          </div>
                            <div class="form-group">
                             <label for="exampleInputPassword1">Password</label>
                             <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password"/>
                            </div>
                                 <div class="form-check">
                                    <input type="checkbox" class="form-check-input" id="exampleCheck1"/>
                                    <label class="form-check-label" for="exampleCheck1">Remember me</label>
                                  </div>
                                  <div className="row">
                                      <div className="col">
                                      <button type="submit" class="btn btn-primary mt-4">LOG IN</button>
                                      </div>
                                      <div className="col">
                                      <p className="forgetpass mt-4"><a href="#">Forget Password</a></p>
                                      </div>
                                  </div>
                                  </form>
             
                                 </div>

                    </div>
                    <div className="col-md-6 mt-5">
                    <div className="register ">
                        <form>
                          <div class="form-group">
                         <label for="exampleInputEmail1">Email address</label>
                         <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email"/ >
                          </div>
                            <div class="form-group">
                             <label for="exampleInputPassword1">Password</label>
                             <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password"/>
                            </div>
                                 
                                  <p className="note"><b>Your personal data will be used to support your experience throughout this website,
                  to manage access to your account, and for other purposes described in our privacy policy.</b></p>
                                    <button type="submit" class="btn btn-primary mt-2">REGISTER</button>
                                  </form>
            
                                 </div>

                    </div>
                </div>
                
            </div>
        )
    }
}

export default MyAccount;

import React, { Component } from 'react'
import authbanner from '../../Images/authbanner.jpg'
export class AuthenticationBanner extends Component {
    render() {
        return (
            <div className="authentication banner py-5">
                 <img src={authbanner} class="img-fluid" alt="Responsive image"  width="1403"/>
            </div>
        )
    }
}

export default AuthenticationBanner;

import React, { Component } from 'react'
import './style.css'
class Shoppage extends Component {
    state={
        products:[
            {
                "_id": "1",
                "title": "GXN (GREENEX NUTRITION) N.O. PUMP – BEST PRE WORKOUT",
                "src": [
                    "https://gogxn.com/wp-content/uploads/2020/12/noPump30_Blueberry.png",
                    // "https://gogxn.com/wp-content/uploads/2020/12/noPump30dise02_final_4K-1.png",
                    "https://gogxn.com/wp-content/uploads/2020/12/noPump30_Blueberry.png",
                    "https://gogxn.com/wp-content/uploads/2020/12/noPump30_Blueberry.png",
                    "https://gogxn.com/wp-content/uploads/2020/12/noPump30_Blueberry.png",
                    
                    "https://gogxn.com/wp-content/uploads/2018/08/IMG-20200926-WA0008.jpg"
                
                  ],
                "description": "UI/UX designing, html css tutorials",
                "content": "Each serving of N.O.  Pump includes Citruline DL malate (1500 mg), Beta Alanine (3000mg), Creatine (1000 mg), and L Arginine (1500 mg) ",
                "price": 23,
                "colors":["red","black","crimson","teal"],
                "count": 1
                }
        
        ],
        index:0
    };
    myRef = React.createRef();
    handleactive= index=>{
        this.setState({index: index})
        const images = this.myRef.current.children;
        for(let i=0; i<images.length; i++){
            images[i].className= images[i].className.replace("active", "");
        }
        images[index].className="active";
        // console.log(this.myRef.current.children)
        // alert(index)
    }

    render() {
        const {products, index} = this.state;
        console.log(products)
        return (
            <div className="shop_page_details">
              { products.map(item =>(
                <div className="shop_container  px-5">
                    <div className="row ">
                        <div className="col-md-6 ">
                        <img src={item.src[index]} class="img-fluid" alt="Responsive image"/>

                            {/* <img src="https://gogxn.com/wp-content/uploads/2020/12/noPump30_Blueberry.png" width="500px" height="500px"/> */}
                       
                        <div class="thumbnail px-3" ref={this.myRef}>
                                       {
                                              item.src.map((img, index)=>(
                                                  <img src={img} alt="/" key={index}
                                                  onClick={() => this.handleactive(index)} class="img-fluid" alt="Responsive image"/>
                                              ))
                                          }  
                                    </div> </div>
                        <div className="col mt-5">
                        <h1 style={{fontsize:"10vw"}}>{item.title}</h1>
                      
                           {/* <button style={{width:'500px', height:"35px", cursor:"text"}}> 30% Cashback</button> */}
                           <ul className="list-md-6 mt-5" >
                           <li>Each serving of N.O.  Pump includes Citruline DL malate (1500 mg), Beta Alanine (3000mg), Creatine (1000 mg), and L Arginine (1500 mg)</li>
                           <li>GXN N.O. Pump pre-workout supplement is specially crafted to increase workout endurance and efficiency.</li>
                           <li>N O. pump includes rich amount of Caffeine (220mg) and vitamin B3 and B6 which helps increases the focus and concentration during workout.</li>
                           <li>N.O. Pump supplement can be preferred 20-30 minutes before you hit the gym and advisable for both men and women. </li></ul>
                           <p>Size:</p>
                           <div className="row mb-2">
                        <button className='chose ml-2'>30 Serving</button>
                        <button className='chose ml-2'>60 Serving</button>
                            </div>
                            <p>Flavour</p>
                            <div className="row">
                        <button className='chose ml-2'>Watermelon</button>
                        <button className='chose ml-2'>Fruit Punch</button>
                        <button className='chose ml-2'>Blueberry</button>
                        <button className='chose ml-2'>Guava</button>
                        <button className='chose ml-2'>Fruit Punch</button>
                        <button className='chose ml-2'>Blueberry</button>
                        <button className='chose ml-2'>Guava</button>
                            </div>

                            <price>Rs.2034</price>
                        </div>
                       
                    </div>
                </div>
                ))  } 
            </div>
        )
    }
}

export default Shoppage

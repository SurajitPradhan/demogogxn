import React from 'react'
import {Grid} from '@material-ui/core';
import Products from './Products/Products';

const product = [
    { id:1, name:'whey', description: 'Best Protein'   },
    { id:1, name:' Gold whey', description: 'Best Protein'   },
    { id:1, name:' Armour whey', description: 'Best Protein'   }
]
const Productdata = () => {
    return (
    <main>
        <Grid container justify="center" spacing={4}>
            {product.map((product)=>(
                <Grid item key={product.id} xs={12} sm={6} md={4} lg={3}>
                    <Products product ={product}/>
                    </Grid>
            ))}
        </Grid>
    </main>
    )
}

export   { Productdata, 
    Grid };
    export default Productdata
    

import React, { Component } from 'react'
import './style.css';
import authbanner from '../../Images/authbanner.jpg'
export class Authenticity extends Component {
    render() {
        return (
            <div className="authenticity">
                <div className="bannerportion">
                    <img src={authbanner} className="img-fluid" width="100%"/>
                </div>
                <div className="auth_steps">
                    <h1 className="check">CHECK YOUR PRODUCT'S AUTHENTICATION</h1>
                    <p className="steps">GXN Believes on the fact that the authenticity of any nutrition 
                    products is the most important element for user’s health. And our continuous & successful 
                    efforts of providing 100% authentic nutrition made us India’s most trusted supplement brand.
                     GXN is certified by all Government and top certification agencies like FSSAI, HACCP, GMP, 
                     & ISO.</p>
                     <p className="step_one"> What is Products Authentication?</p>
                     <p className="steps">Authentication is a process of validating the product that it is original product made by the 
                         respective brand and not a counterfeit product.</p>
                         <p className="step_one">How to Authenticate GXN Products?</p>
                         <p className="steps"><b>Step 1:</b> Find the QR code affixed at the protection seal, inside cap of the box.</p>
                         <p className="steps"><b>Step 2:</b> Scan the QR code with any QR scanner</p>
                         <p className="steps"><b>Step 3:</b> If the product is originally manufactured from GXN it will show you a message 
                         <b>“Your product is 100% authentic”</b> on your phone’s screen. If the product is counterfeit and not manufactured by GXN, it will not authenticate over scanning .On Scanning the QR code Along with 
                         the authentication you will also get to see the product serial number and manufacturing date of the products.</p>
                         <p className="step_one">Why Authentication is important?</p>
                         <p className="steps">Fake products & bogus claim could be a threat to consumer health. The increasing level of counterfeit activities in all sectors poses a huge challenge to industry and governments around the world. Especially with today‘s online sales & purchase models, where e-commerce 
                         platforms facilitate the initial transaction between buyers and sellers without any physical appearance.</p>
                         <p className="steps">This is where the process of authentication prevents any further mishap if your products turned out to be not genuine. It is not only helpful
                              for the health & wellness concern but also gives mental peace even if the product is authentic.</p>
                              <p className="steps">We at GXN keep product authenticity process as on top priority for making the users confident before 
                                  using any product in the name of GXN.</p>
                                  <p  className="step_one">What is the role of the “Smooth Authentication Process” for brand management?</p>
                                  <p className="steps">The duplicity of products & illicit trade across all market sectors is a developing issue and it is not only
                                       costing the economic loss of genuine brand but also hampering the reputation of the brand on a wider level.
                                        This is where technological advancement comes to make it easier for the buyers to validate the products,
                                         whether this is genuine or not.
                                       Easier & smooth authentication process provides better transparency & consistent trust with the brand.</p>
                                       <p className="steps"><b>Disclaimer:</b>All GXN products are manufactured at FSSAI approved manufacturing facilities and are not intended to diagnose, 
                                       treat, cure, or prevent any disease. Please read product packaging carefully before purchase and use. The 
                                       information/articles on GXN’s products label or domain <a href="">(www.gogxn.com)</a> is provided for informational purpose
                                        only and is not meant to substitute for the advice provided by your doctor or other healthcare professional. 
                                           These statements are not ratified by any government agency and are for general guidance only.</p>
                </div>
            </div>
        )
    }
}

export default Authenticity;

import React, { Component } from 'react'
import './style.css'

const Card=(props)=> {
   
        return (
            <div class="card">
              <a class="icon pl-3 "> <i class="fa fa-heart-o fa-center" aria-hidden="true"  ></i></a> 
             <div className="div px-5" >

               <center>
  <img class="card-img-top homeproductimg" src={props.image} alt="Responsive image" /></center></div>
  {/* <div class="card-body"> */}
  
    <a href="#"><p class="card-title product_name mt-3 pl-3">{props.name} </p></a>
    <div className="icons ">
    <div class="row"> 

    <i class="fa fa-star" aria-hidden="true"style={{color:"#F7C434"}}></i>
    <i class="fa fa-star" aria-hidden="true" style={{color:"#F7C434"}}></i>
    <i class="fa fa-star" aria-hidden="true" style={{color:"#F7C434"}}></i>
    <i class="fa fa-star" aria-hidden="true" style={{color:"#F7C434"}}></i>
    <i class="fa fa-star" aria-hidden="true" style={{color:"#F7C434"}}></i>

</div></div>
<p class="card-text px-3">Rs.{props.price}</p>

  </div>
 
 
// </div>

 

        )
    }


export default Card

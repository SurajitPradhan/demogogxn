import React from 'react';
import ReactDOM from 'react-dom';
import './style.css';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import axios from 'axios'
import Card from '../Card';

import whey from '../../Images/wheystak.jpg'
// import Card from './Card';
const options = {
  margin: -10,
 margin: 15,  
  // padding: 50,
  responsiveClass: true,
  nav: true,
  autoplay: false,
 loop:true,
  smartSpeed: 1000,
  responsive: {
      0: {
          items: 1,
      },
      400: {
          items: 1,
      },
      600: {
          items: 1,
      },
      700: {
          items: 3,
      },
      1000: {
          items: 4,
      }
  },
};

class NewArrival extends React.Component {
    constructor(props) {
        super(props)
    
        this.state =

         {
            name:[],
            text:[],
            image:[],
            id:[]     
        }
       
    }
    
    componentDidMount=()=>{
        axios.get('http://127.0.0.1:8000/carsouel/')
        .then((res)=>{
          for (var i = 0; i < res.data.length; i++)
         
          this.setState({
            name: [...this.state.name, res.data[i].name],
            text: [...this.state.text, res.data[i].text],
            id: [...this.state.id, res.data[i].id],
            image: [...this.state.image, res.data[i].image],
            // data:[res.data]
          })
    
          console.log(res)
        })
      }
  render() {
      
     const data= this.state;
     console.log(data)
       return (
<div class="carousel px-4"><center>

 {/* {...this.state.name.map(name=><p key>{name.price}
    </p> */}
   
    <h2 className=" proteins py-1"> NEW ARRIVAL's </h2>
  
  </center>


{/* {this.state.name.map(name=><p key>{name.price} </p>)} */}
       
  <OwlCarousel className="slider-items owl-carousel px-5" {...options}>

                      <div class="item"><Card name={this.state.name[0]} image={this.state.image[0]} price={this.state.text[0]}/></div>
                      <div class="item"><Card name={this.state.name[1]} image={this.state.image[1]}  price={this.state.text[1]} /></div>
                      <div class="item"><Card name={this.state.name[2]} image={this.state.image[2]}  price={this.state.text[2]}/></div>
                      <div class="item"><Card name={this.state.name[3]} image={this.state.image[3]}  price={this.state.text[3]}/></div>
                      <div class="item"><Card name={this.state.name[0]} image={this.state.image[0]}  price={this.state.text[0]}/></div>
                      <div class="item"><Card name={this.state.name[1]} image={this.state.image[1]}  price={this.state.text[1]}/></div>
                      <div class="item"><Card name={this.state.name[2]} image={this.state.image[2]}  price={this.state.text[2]}/></div>
                      <div class="item"><Card name={this.state.name[3]} image={this.state.image[3]}  price={this.state.text[3]}/></div>
                      <div class="item"><Card name={this.state.name[0]} image={this.state.image[0]}  price={this.state.text[0]}/></div>
                      
  

                                   
                  </OwlCarousel> 
</div>
)   
};
}

export default NewArrival;



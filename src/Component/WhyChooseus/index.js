import axios from 'axios'
import React, { Component } from 'react'
import './style.css'
import image1 from '../../Images/reasonone.png'
import image2 from '../../Images/reasontwo.png'
import image3 from '../../Images/reasonthree.png'
import image4 from '../../Images/reasonfour.png'
import image5 from '../../Images/reasonfive.png'
import image6 from '../../Images/reasonsix.png'

 class WhyChooseus extends Component {
     constructor(props) {
         super(props)
     
         this.state = {
             id:[],
             
              image:[]
         }
     }
   
     componentDidMount(){
         axios.get('http://127.0.0.1:8000/whychooseus/')
         .then((res)=>{
            for (var i = 0; i < res.data.length; i++)
           
            this.setState({
              
           
              id: [...this.state.id, res.data[i].id],
              image: [...this.state.image, res.data[i].image]
      
            })
      
            console.log(res)
         })
     }
    render() {
        return (
            <div>
                <h3 className="why_choose_us px-1">Why Choose us?</h3>
                <h5 className="why_choose px-3 mt-3">You can count on us & we mean it!</h5>
                <p className="why_chose_gxn px-4 py-1">Our products are formulated with International standards and are highly result oriented. Made with premium ingredients and are highly rich in protein.</p>
                <div className="whychooseus_images">
                <div class="Whychoseus_container px-5">
                     <div class="row">
                       
                               <div class="col-sm"><center><br/>
                         <img src={this.state.image[0]} class="img-fluid" alt="Responsive image"/>
                         <strong>
                         <h6 className="image_para">Premium Grade Products</h6></strong></center>
                            </div>
                            <div class="col-sm"><center><br/>
                         <img src={this.state.image[1]} class="img-fluid" alt="Responsive image"/>
                         <strong>
                         <h6 className="image_para">High Protein Composition</h6></strong></center>
                            </div>
                            <div class="col-sm"><center><br/>
                         <img src={this.state.image[2]} class="img-fluid" alt="Responsive image"/>
                         <strong>
                         <h6 className="image_para">No Steroids Added</h6></strong></center>
                            </div>
                            <div class="col-sm"><center><br/>
                         <img src={this.state.image[3]} class="img-fluid" alt="Responsive image"/>
                         <strong>
                         <h6 className="image_para">Low Sugar & Carbs</h6></strong></center>
                            </div>
                            <div class="col-sm"><center><br/>
                         <img src={this.state.image[4]} class="img-fluid" alt="Responsive image"/>
                         <strong>
                         <h6 className="image_para">Genuine & Authentic</h6></strong></center>
                            </div>
                       <div class="col-sm"><center><br/>
                    <img src={this.state.image[5]} class="img-fluid" alt="Responsive image"/>
                    <b>
                    <h6 className="image_para">Unbelievable Prices</h6></b></center>
                                   </div>
                 </div>
               </div>
                </div>
                
                
                            </div>
        )
    }
}

export default WhyChooseus;

import React, { Component } from 'react'
import careers from '../../Images/careers.jpg'

export class Careers extends Component {
    render() {
        return (
            <div className="career_details " ><div>
                <img className="careers px-5 py-5"src={careers} alt="rep" width="1422"/></div>
              
                <p className="para px-5">An inclusive work culture is the first and promising philosophy of Greenex Nutrition.</p>
                <p className="para px-5">Beginners are guided by the core professionals in a cumulative way where they could succeed in
                     building the rigid base of a professional career for flourishing growth.</p>
                <p className="para px-5">Our tiny initiatives to major goals are driven and completed by the ideas and strategies of 
                    our employees. Their recommendations are heard, polished and shaped by the managements very
                     constructively to encourage the ideas of all.</p>
                     <p className="para px-5">Most conducive atmosphere which would inspire and motivate you for developing your
                          potential to the fullest, which you would definitely like to do.</p>
                          <p className="para px-5">We are committed to building lifelong relationships with our professionals whose 
                              contributions towards building the GXN are commendable and praiseworthy.</p>
                              <p className="para px-5">Salary and perks are totally based on your results/achievements. Unlimited
                                   opportunities for your professional & personal growth.</p>
                                   <p className="para px-5">Let’s Join the dynamic team of experts and undisputed leader for the thriving experience to give your career a justifiable recognition</p>
                                   
                                   <p className="opportunitites px-5 py-2   ">CURRENT OPPORTUNITIES:-</p>
                                   
                                   </div>
        )
    }
}

export default Careers


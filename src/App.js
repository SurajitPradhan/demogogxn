import logo from './logo.svg';

import Container from './Container';


import Product from './Component/Product';
import Productdata from './Component/Productdata/Productdata';
import Shop from './Component/Shop/Shop';
import Details from './Component/Details/Details';
import Shoppage from './Component/ShopTest/Shoppage';
import Home from './Container/Home';
import Banner from './Component/Banner/index.js';
import ProteinProduct from './Component/ProteinProduct/carousel';
import WhyChooseus from './Component/WhyChooseus';
import Homepageblog from './Component/BlogCard/BlogCarousel';
import Footer from './Component/Footer';
import Topbarstrip from './Component/Topbarstrip';
import Footerstrip from './Component/Footerstrip';
import Careers from './Component/Careers';
import Terms from './Component/Terms';
import RefundPolicy from './Component/RefundPolicy';
import PrivacyPolicy from './Component/PrivacyPolicy';
import CustomersSupport from './Component/CustomersSupport';
import MyAccount from './Component/MyAccount';
import MassGainers from './Component/MassGainers';
import PrePost from './Component/PrePost';
import WorkOut from './Component/WorkOut';
import FatBurner from './Component/FatBurner';
import Accessories from './Component/Accessories';

import Accordian from './Component/Accordian';
import Authenticity from './Component/Authenticity';
import NewArrival from './Component/NewArrival';
import ProductDetails from './Component/ProductDetails';
import AuthenticationBanner from './Component/AuthenticationBanner';
import Mobo from './Component/BlogCard/Mobo';
import AfterNav from './Component/AfterNav'




function App() {
  return (
    <div className="App">
      <Topbarstrip/>
    
      <AfterNav/>
    
    {/* <ProductDetails/> */}
   

      {/* <Accordian/> */}
      {/* <Authenticity/>
     
      <NewArrival/> */}
    {/* <Topbarstrip/>
    <Terms/> */}
{/* <Topbarstrip/>
<Home/> */}
{/* <Home/>
<Footerstrip/>
<Careers/>
<Terms/>
<RefundPolicy/>
<CustomersSupport/>
<PrivacyPolicy/>
<CustomersSupport/>
<Banner/>
<MyAccount/>
<MassGainers/>
<PrePost/>
<WorkOut/>
<FatBurner/>
<Accessories/> */}
{/* <PrePost/> */}

    </div>
  );
}

export default App;
